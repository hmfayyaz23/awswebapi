﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.Models;

namespace QuranCommunityAWS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DuasController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public DuasController(QuranCommunityContext context)
        {
            _context = context;
        }

        // GET: api/Duas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Dua>>> GetDuas()
        {
            return await _context.Duas.ToListAsync();
        }

        // GET: api/Duas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Dua>> GetDua(int id)
        {
            var dua = await _context.Duas.FindAsync(id);

            if (dua == null)
            {
                return NotFound();
            }

            return dua;
        }

        // PUT: api/Duas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDua(int id, Dua dua)
        {
            if (id != dua.DuaId)
            {
                return BadRequest();
            }

            _context.Entry(dua).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DuaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Duas
        [HttpPost]
        public async Task<ActionResult<Dua>> PostDua(Dua dua)
        {
            _context.Duas.Add(dua);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDua", new { id = dua.DuaId }, dua);
        }

        // DELETE: api/Duas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Dua>> DeleteDua(int id)
        {
            var dua = await _context.Duas.FindAsync(id);
            if (dua == null)
            {
                return NotFound();
            }

            _context.Duas.Remove(dua);
            await _context.SaveChangesAsync();

            return dua;
        }

        private bool DuaExists(int id)
        {
            return _context.Duas.Any(e => e.DuaId == id);
        }
    }
}
