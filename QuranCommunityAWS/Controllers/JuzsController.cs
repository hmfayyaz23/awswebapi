﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.Models;

namespace QuranCommunityAWS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JuzsController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public JuzsController(QuranCommunityContext context)
        {
            _context = context;
        }

        // GET: api/Juzs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Juz>>> GetJuzs()
        {
            return await _context.Juzs.ToListAsync();
        }

        // GET: api/Juzs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Juz>> GetJuz(int id)
        {
            var juz = await _context.Juzs.FindAsync(id);

            if (juz == null)
            {
                return NotFound();
            }

            return juz;
        }

        // PUT: api/Juzs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutJuz(int id, Juz juz)
        {
            if (id != juz.Id)
            {
                return BadRequest();
            }

            _context.Entry(juz).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JuzExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Juzs
        [HttpPost]
        public async Task<ActionResult<Juz>> PostJuz(Juz juz)
        {
            _context.Juzs.Add(juz);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetJuz", new { id = juz.Id }, juz);
        }

        // DELETE: api/Juzs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Juz>> DeleteJuz(int id)
        {
            var juz = await _context.Juzs.FindAsync(id);
            if (juz == null)
            {
                return NotFound();
            }

            _context.Juzs.Remove(juz);
            await _context.SaveChangesAsync();

            return juz;
        }

        private bool JuzExists(int id)
        {
            return _context.Juzs.Any(e => e.Id == id);
        }
    }
}
