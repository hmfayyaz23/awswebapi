﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.Models;

namespace QuranCommunityAWS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AyahTextsController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public AyahTextsController(QuranCommunityContext context)
        {
            _context = context;
        }

        // GET: api/AyahTexts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AyahText>>> GetAyahText()
        {
            return await _context.AyahText.ToListAsync();
        }

        // GET: api/AyahTexts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AyahText>> GetAyahText(int id)
        {
            var ayahText = await _context.AyahText.FindAsync(id);

            if (ayahText == null)
            {
                return NotFound();
            }

            return ayahText;
        }

        // PUT: api/AyahTexts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAyahText(int id, AyahText ayahText)
        {
            if (id != ayahText.Id)
            {
                return BadRequest();
            }

            _context.Entry(ayahText).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AyahTextExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AyahTexts
        [HttpPost]
        public async Task<ActionResult<AyahText>> PostAyahText(AyahText ayahText)
        {
            _context.AyahText.Add(ayahText);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAyahText", new { id = ayahText.Id }, ayahText);
        }

        // DELETE: api/AyahTexts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AyahText>> DeleteAyahText(int id)
        {
            var ayahText = await _context.AyahText.FindAsync(id);
            if (ayahText == null)
            {
                return NotFound();
            }

            _context.AyahText.Remove(ayahText);
            await _context.SaveChangesAsync();

            return ayahText;
        }

        private bool AyahTextExists(int id)
        {
            return _context.AyahText.Any(e => e.Id == id);
        }
    }
}
