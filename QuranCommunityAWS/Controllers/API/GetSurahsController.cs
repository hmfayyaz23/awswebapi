﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.ModelsRes;
using QuranCommunityAWS.ModelsRes.Surah;

namespace QuranCommunityAWS.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetSurahsController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public GetSurahsController(QuranCommunityContext context)
        {
            _context = context;
        }


        // POST: api/GetSurahs
        [HttpPost]
        public async Task<ActionResult<MainResponse<List<SurahResponse>>>> GetSurahsAPI(GetSurahParam param)
        {
            MainResponse<List<SurahResponse>> res = new MainResponse<List<SurahResponse>>();

            try{
                if (param.Id <= 0 || param.Id > 114)
                {
                    res.SetCustomError(201 , "Surah must be between 1 and 114 inclusive.");
                    return res;
                }

                if (param.Limit <= 0)
                {
                    res.SetCustomError(201, "Limit can not be negative or zero");
                    return res;
                }


                var surahs = await _context.Surahs.Where(x => (x.SurahNumber >= param.Id) && (x.SurahNumber < (param.Id + param.Limit))).ToListAsync();
                if (surahs == null)
                {
                    res.SetDefaultError();
                    return res;
                }

                List<SurahResponse> mSurahs = new List<SurahResponse>();
                foreach (Models.Surah surah in surahs) {
                    mSurahs.Add(new ModelsRes.Surah.SurahResponse(surah.Id, surah.SurahNumber, surah.NameInEnglish, surah.TranslationinEnglish, surah.NameInArabic, surah.StartAyahId, surah.EndAyahId, surah.Origin));
                }

                res.Data = mSurahs;

                return res;
            }
            catch
            {
                res.SetCustomError(401, "Something Went Wrong, please contact support");
                return res;
            }
        }

    }
}