﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.Models;
using QuranCommunityAWS.ModelsRes;
using QuranCommunityAWS.ModelsRes.GetRecommendedVideos;
using QuranCommunityAWS.ModelsRes.Surah;

namespace QuranCommunityAWS.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetRecommendedVideosForAyahController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public GetRecommendedVideosForAyahController(QuranCommunityContext context)
        {
            _context = context;
        }


        // POST: api/GetRecommendedVideosForAyah
        [HttpPost]
        public async Task<ActionResult<MainResponse<List<AyahRecommendedVideos>>>> GetRecommendedVideosForAyah(GetRecommendedVideosParam param)
        {
            MainResponse<List<AyahRecommendedVideos>> res = new MainResponse<List<AyahRecommendedVideos>>();

            try
            {
                if (param.Ids.Where(x => x <= 0 || x > 6236).Count() > 0)
                {
                    res.SetCustomError(201, "Ayah must be between 1 and 6236 inclusive.");
                    return res;
                }

                res.Data = new List<AyahRecommendedVideos>();
                foreach (int id in param.Ids) {
                    AyahRecommendedVideos ARV = new AyahRecommendedVideos();
                    ARV.AyahNumber = id;
                    ARV.VideoIds = await _context.RecommendedVideos.Where(x => x.AyahNumber == id).Select(x => x.VideoId).ToListAsync();
                    res.Data.Add(ARV);
                }

                return res;
            }
            catch
            {
                res.SetCustomError(401, "Something Went Wrong, please contact support");
                return res;
            }
        }
    }
}