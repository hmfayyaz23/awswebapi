﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.ModelsRes;
using QuranCommunityAWS.ModelsRes.QuranFeedModels;

namespace QuranCommunityAWS.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetQuranFeedController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public GetQuranFeedController(QuranCommunityContext context)
        {
            _context = context;
        }

        // POST: api/GetQuranFeed
        [HttpPost]
        public async Task<ActionResult<MainResponse<List<QuranFeed>>>> GetQuranFeedAPI(GetQuranFeedParam param)
        {

            MainResponse<List<QuranFeed>> res = new MainResponse<List<QuranFeed>>();

            try
            {
                if (param.Id <= 0 || param.Id > 6236)
                {
                    res.SetCustomError(201, "Ayah must be between 1 and 6236 inclusive.");
                    return res;
                }

                if (param.Limit <= 0)
                {
                    res.SetCustomError(201, "Limit can not be negative or zero");
                    return res;
                }


                var languages = await _context.Languages.Where(x => param.LanguageIds.Contains(x.LanguageId)).ToListAsync();
                if (languages == null || languages.Count != param.LanguageIds.Count)
                {
                    res.SetCustomError(201, "Language id not found");
                    return res;
                }

            
                res.Data = new List<QuranFeed>();
                for (int i = param.Id; i < (param.Id + param.Limit); i++) {
                    if (i > 6236) break;

                    QuranFeed QF = new QuranFeed();

                    // check has recommended videos
                    var videosCount = await _context.RecommendedVideos.Where(x => x.AyahNumber == i).CountAsync();
                    if (videosCount > 0) {
                        QF.HasRecommenedVideo = true;
                    }
                    else {
                        QF.HasRecommenedVideo = false;
                    }

                    // Get Ayah Data
                    var ayah = await _context.Ayahs.FindAsync(i);
                    if (ayah == null) {
                        res.Data = null;
                        res.SetCustomError(201, "Data of some Ayahs are missing in database");
                        return res;
                    }

                    // Prepare Display Ayah Text List
                    List<DisplayAyahText> displayAyahText = new List<DisplayAyahText>();
                    foreach (int languageId in param.LanguageIds)
                    {
                        Models.AyahText ayahText = await _context.AyahText.FirstOrDefaultAsync(x => (x.AyahNumber == i && x.LanguageId == languageId));

                        if (ayahText == null)
                        {
                            displayAyahText.Add(new DisplayAyahText(RandomId(), i, "Data not found", languageId));
                        }
                        else
                        {
                            displayAyahText.Add(new DisplayAyahText(ayahText.Id, ayahText.AyahNumber, ayahText.Text, ayahText.LanguageId));
                        }

                    }

                    // Prepare Display Ayah Model
                    DisplayAyahModel displayAyahModel = new DisplayAyahModel(ayah.Id, ayah.AyahNumber, ayah.DuaId, ayah.SurahNumber, ayah.JuzNumber, displayAyahText);

                    QF.DisplayAyahModel = displayAyahModel;

                    res.Data.Add(QF);
                }

                return res;
            }
            catch
            {
                res.SetCustomError(401, "Something Went Wrong, please contact support");
                return res;
            }
        }

        // Generate a random number between two numbers
        public int RandomId()
        {
            Random random = new Random();
            return random.Next(30000, 50000);
        }
    }
}