﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.ModelsRes;
using QuranCommunityAWS.ModelsRes.GetRecommendedVideos;

namespace QuranCommunityAWS.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetRecommendedVideosForSurahController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public GetRecommendedVideosForSurahController(QuranCommunityContext context)
        {
            _context = context;
        }


        // POST: api/GetRecommendedVideosForSurah
        [HttpPost]
        public async Task<ActionResult<MainResponse<List<SurahRecommendedVideos>>>> GetRecommendedVideosForSurah(GetRecommendedVideosParam param)
        {
            MainResponse<List<SurahRecommendedVideos>> res = new MainResponse<List<SurahRecommendedVideos>>();

            try
            {
                if (param.Ids.Where(x => x <= 0 || x > 114).Count() > 0)
                {
                    res.SetCustomError(201, "Surah must be between 1 and 114 inclusive.");
                    return res;
                }

                res.Data = new List<SurahRecommendedVideos>();
                foreach (int id in param.Ids)
                {
                    SurahRecommendedVideos ARV = new SurahRecommendedVideos();
                    ARV.SurahNumber = id;
                    ARV.VideoIds = await _context.RecommendedVideos.Where(x => x.SurahNumber == id).Select(x => x.VideoId).ToListAsync();
                    res.Data.Add(ARV);
                }

                return res;
            }
            catch
            {
                res.SetCustomError(401, "Something Went Wrong, please contact support");
                return res;
            }
        }
    }
}