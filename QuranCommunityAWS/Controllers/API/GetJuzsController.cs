﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.ModelsRes;
using QuranCommunityAWS.ModelsRes.Juz;

namespace QuranCommunityAWS.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetJuzsController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public GetJuzsController(QuranCommunityContext context)
        {
            _context = context;
        }

        // POST: api/GetJuzs
        [HttpPost]
        public async Task<ActionResult<MainResponse<IEnumerable<JuzResponse>>>> GetJuzs(GetJuzParam param)
        {
            MainResponse<IEnumerable<JuzResponse>> response = new MainResponse<IEnumerable<JuzResponse>>();

            try {

                if (param.Id <= 0 || param.Id > 30)
                {
                    response.SetCustomError(201, "Juz must be between 1 and 30 inclusive.");
                    return response;
                }

                if (param.Limit <= 0 || (param.Id + param.Limit) > 31)
                {
                    response.SetCustomError(201, "Juz must be between 1 and 30 inclusive.");
                    return response;
                }

                var juzs = await _context.Juzs.Where(x => (x.JuzNumber >= param.Id) && (x.JuzNumber < (param.Id + param.Limit))).ToListAsync();
                if (juzs == null)
                {
                    response.SetDefaultError();
                    return response;
                }

                List<JuzResponse> mJuzs = new List<JuzResponse>();
                foreach (Models.Juz juz in juzs)
                {
                    mJuzs.Add(new JuzResponse(juz.Id, juz.JuzNumber, juz.NameInEnglish, juz.NameInArabic, juz.StartAyahId, juz.EndAyahId, juz.StartSurahId));
                }

                response.Data = mJuzs;

                return response;
            }
            catch
            {
                response.SetCustomError(401, "Something Went Wrong, please contact support");
                return response;
            }

        }
    }
}