﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.ModelsRes;
using QuranCommunityAWS.ModelsRes.Dua;

namespace QuranCommunityAWS.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetDuaController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public GetDuaController(QuranCommunityContext context)
        {
            _context = context;
        }

        // POST: api/GetDuas
        [HttpPost]
        public async Task<ActionResult<MainResponse<DuaResponse>>> PostDuas(GetDuaParam param)
        {
            MainResponse<DuaResponse> response = new MainResponse<DuaResponse>();

            try { 
                if (param.Id <= 0 || param.Id > 6236)
                {
                    response.SetCustomError(201, "Dua can not be negative or zero");
                    return response;
                }

                var duas = await _context.Duas.FindAsync(param.Id);
                if (duas == null)
                {
                    response.SetDefaultError();
                    return response;
                }

                response.Data = new DuaResponse(duas.DuaId, duas.AyahNumber, duas.Title, duas.Details);

                return response;
            }
            catch
            {
                response.SetCustomError(401, "Something Went Wrong, please contact support");
                return response;
            }
}
    }
}