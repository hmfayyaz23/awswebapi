﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.Models;

namespace QuranCommunityAWS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecommendedVideosController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public RecommendedVideosController(QuranCommunityContext context)
        {
            _context = context;
        }

        // GET: api/RecommendedVideos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RecommendedVideos>>> GetRecommendedVideos()
        {
            return await _context.RecommendedVideos.ToListAsync();
        }

        // GET: api/RecommendedVideos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RecommendedVideos>> GetRecommendedVideos(int id)
        {
            var recommendedVideos = await _context.RecommendedVideos.FindAsync(id);

            if (recommendedVideos == null)
            {
                return NotFound();
            }

            return recommendedVideos;
        }

        // PUT: api/RecommendedVideos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRecommendedVideos(int id, RecommendedVideos recommendedVideos)
        {
            if (id != recommendedVideos.Id)
            {
                return BadRequest();
            }

            _context.Entry(recommendedVideos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecommendedVideosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RecommendedVideos
        [HttpPost]
        public async Task<ActionResult<RecommendedVideos>> PostRecommendedVideos(RecommendedVideos recommendedVideos)
        {
            _context.RecommendedVideos.Add(recommendedVideos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRecommendedVideos", new { id = recommendedVideos.Id }, recommendedVideos);
        }

        // DELETE: api/RecommendedVideos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RecommendedVideos>> DeleteRecommendedVideos(int id)
        {
            var recommendedVideos = await _context.RecommendedVideos.FindAsync(id);
            if (recommendedVideos == null)
            {
                return NotFound();
            }

            _context.RecommendedVideos.Remove(recommendedVideos);
            await _context.SaveChangesAsync();

            return recommendedVideos;
        }

        private bool RecommendedVideosExists(int id)
        {
            return _context.RecommendedVideos.Any(e => e.Id == id);
        }
    }
}
