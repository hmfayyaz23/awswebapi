﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.Models;

namespace QuranCommunityAWS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AyahsController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public AyahsController(QuranCommunityContext context)
        {
            _context = context;
        }

        // GET: api/Ayahs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ayah>>> GetAyahs()
        {
            return await _context.Ayahs.ToListAsync();
        }

        // GET: api/Ayahs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Ayah>> GetAyah(int id)
        {
            var ayah = await _context.Ayahs.FindAsync(id);

            if (ayah == null)
            {
                return NotFound();
            }

            return ayah;
        }

        // PUT: api/Ayahs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAyah(int id, Ayah ayah)
        {
            if (id != ayah.Id)
            {
                return BadRequest();
            }

            _context.Entry(ayah).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AyahExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Ayahs
        [HttpPost]
        public async Task<ActionResult<Ayah>> PostAyah(Ayah ayah)
        {
            _context.Ayahs.Add(ayah);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAyah", new { id = ayah.Id }, ayah);
        }

        // DELETE: api/Ayahs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Ayah>> DeleteAyah(int id)
        {
            var ayah = await _context.Ayahs.FindAsync(id);
            if (ayah == null)
            {
                return NotFound();
            }

            _context.Ayahs.Remove(ayah);
            await _context.SaveChangesAsync();

            return ayah;
        }

        private bool AyahExists(int id)
        {
            return _context.Ayahs.Any(e => e.Id == id);
        }
    }
}
