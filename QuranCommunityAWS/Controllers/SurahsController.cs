﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.DAL;
using QuranCommunityAWS.Models;

namespace QuranCommunityAWS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurahsController : ControllerBase
    {
        private readonly QuranCommunityContext _context;

        public SurahsController(QuranCommunityContext context)
        {
            _context = context;
        }

        // GET: api/Surahs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Surah>>> GetSurahs()
        {
            return await _context.Surahs.ToListAsync();
        }

        // GET: api/Surahs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Surah>> GetSurah(int id)
        {
            var surah = await _context.Surahs.FindAsync(id);

            if (surah == null)
            {
                return NotFound();
            }

            return surah;
        }

        // PUT: api/Surahs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSurah(int id, Surah surah)
        {
            if (id != surah.Id)
            {
                return BadRequest();
            }

            _context.Entry(surah).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SurahExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Surahs
        [HttpPost]
        public async Task<ActionResult<Surah>> PostSurah(Surah surah)
        {
            _context.Surahs.Add(surah);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSurah", new { id = surah.Id }, surah);
        }

        // DELETE: api/Surahs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Surah>> DeleteSurah(int id)
        {
            var surah = await _context.Surahs.FindAsync(id);
            if (surah == null)
            {
                return NotFound();
            }

            _context.Surahs.Remove(surah);
            await _context.SaveChangesAsync();

            return surah;
        }

        private bool SurahExists(int id)
        {
            return _context.Surahs.Any(e => e.Id == id);
        }
    }
}
