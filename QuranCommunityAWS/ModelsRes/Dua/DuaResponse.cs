﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes.Dua
{
    public class DuaResponse
    {
        public int DuaId { get; set; }
        public int AyahNumber { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }

        public DuaResponse(int duaId, int ayahNumber, string title, string details)
        {
            DuaId = duaId;
            AyahNumber = ayahNumber;
            Title = title;
            Details = details;
        }
    }
}
