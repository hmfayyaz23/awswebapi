﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes.GetRecommendedVideos
{
    public class SurahRecommendedVideos
    {
        public int SurahNumber { get; set; }
        public List<string> VideoIds { get; set; }
    }
}
