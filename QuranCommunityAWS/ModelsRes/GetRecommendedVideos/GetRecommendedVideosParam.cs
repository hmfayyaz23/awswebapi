﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes.GetRecommendedVideos
{
    public class GetRecommendedVideosParam
    {
        public List<int> Ids { get; set; }
    }
}
