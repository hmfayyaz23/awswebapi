﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes.Juz
{
    public class JuzResponse
    {
        public int Id { get; set; }
        public int JuzNumber { get; set; }
        public string NameInEnglish { get; set; }
        public string NameInArabic { get; set; }
        public int StartAyahId { get; set; }
        public int EndAyahId { get; set; }
        public int StartSurahId { get; set; }

        public JuzResponse(int id, int juzNumber, string nameInEnglish, string nameInArabic, int startAyahId, int endAyahId, int startSurahId)
        {
            Id = id;
            JuzNumber = juzNumber;
            NameInEnglish = nameInEnglish;
            NameInArabic = nameInArabic;
            StartAyahId = startAyahId;
            EndAyahId = endAyahId;
            StartSurahId = startSurahId;
        }
    }
}
