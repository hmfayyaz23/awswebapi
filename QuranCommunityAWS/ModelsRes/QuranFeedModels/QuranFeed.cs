﻿using System;
using System.Collections.Generic;

namespace QuranCommunityAWS.ModelsRes.QuranFeedModels
{
    public class QuranFeed
    {
        public Boolean HasRecommenedVideo { get; set; }
        public DisplayAyahModel DisplayAyahModel { get; set; }
    }
}
