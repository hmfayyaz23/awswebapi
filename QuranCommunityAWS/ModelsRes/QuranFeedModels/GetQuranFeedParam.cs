﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes.QuranFeedModels
{
    public class GetQuranFeedParam
    {
        public int Id { get; set; }
        public int Limit { get; set; }
        public List<int> LanguageIds { get; set; }
    }
}
