﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes.QuranFeedModels
{
    public class DisplayAyahModel
    {
        public int Id { get; set; }
        public int AyahNumber { get; set; }
        public int DuaId { get; set; }
        public int SurahNumber { get; set; }
        public int JuzNumber { get; set; }
        public List<DisplayAyahText> AyahText { get; set; }

        public DisplayAyahModel(int id, int ayahNumber, int duaId, int surahNumber, int juzNumber, List<DisplayAyahText> ayahText)
        {
            Id = id;
            AyahNumber = ayahNumber;
            DuaId = duaId;
            SurahNumber = surahNumber;
            JuzNumber = juzNumber;
            AyahText = ayahText;
        }
    }
}
