﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes.QuranFeedModels
{
    public class DisplayAyahText
    {
        public int Id { get; set; }
        public int AyahNumber { get; set; }
        public string Text { get; set; }
        public int LanguageId { get; set; }

        public DisplayAyahText(int id, int ayahNumber, string text, int languageId)
        {
            Id = id;
            AyahNumber = ayahNumber;
            Text = text;
            LanguageId = languageId;
        }
    }
}
