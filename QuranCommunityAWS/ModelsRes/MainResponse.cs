﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes
{
    public class MainResponse<T>
    {
        public MainResponse() {
            this.Code = 200;
            this.Message = "Successful";
        }

        public T Data { get; set; }
        public int Code { get; set; }
        public String Message { get; set; }

        public void SetDefaultError()
        {
            this.Code = 400;
            this.Message = "Failure";
        }

        public void SetCustomError(int code, String msg)
        {
            this.Code = code;
            this.Message = msg;
        }
    }
}
