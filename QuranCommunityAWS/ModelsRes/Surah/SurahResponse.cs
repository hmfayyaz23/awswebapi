﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static QuranCommunityAWS.Models.Surah;

namespace QuranCommunityAWS.ModelsRes.Surah
{
    public class SurahResponse
    {

        public int Id { get; set; }
        public int SurahNumber { get; set; }
        public string NameInEnglish { get; set; }
        public string TranslationinEnglish { get; set; }
        public string NameInArabic { get; set; }
        public int StartAyahId { get; set; }
        public int EndAyahId { get; set; }
        public OriginType Origin { get; set; }

        public SurahResponse(int id, int surahNumber, string nameInEnglish, string translationinEnglish, string nameInArabic, int startAyahId, int endAyahId, OriginType origin)
        {
            Id = id;
            SurahNumber = surahNumber;
            NameInEnglish = nameInEnglish;
            TranslationinEnglish = translationinEnglish;
            NameInArabic = nameInArabic;
            StartAyahId = startAyahId;
            EndAyahId = endAyahId;
            Origin = origin;
        }
    }
}
