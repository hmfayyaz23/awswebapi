﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.ModelsRes.Surah
{
    public class GetSurahParam
    {
        public int Id { get; set; }
        public int Limit { get; set; }
    }
}
