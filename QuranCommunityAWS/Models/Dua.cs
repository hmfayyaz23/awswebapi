﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.Models
{
    public class Dua
    {
        public int DuaId { get; set; }
        public int AyahNumber { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
    }
}

//DuaId : Int(primary key)
//AyahNumber  : int (foreign key .  For which ayah this translation) (original ayah in Quran)
//Title : String  (detail of dua) ) 
//Details : String  (when should recite this dua) 
