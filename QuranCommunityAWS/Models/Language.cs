﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.Models
{
    public class Language
    {
        public enum LanguageStyleOptions
        {
            LTR, RTL
        }

        public int LanguageId { get; set; }
        public string TranslationNameInEnglish { get; set; }
        public string TranslationNameInLocal { get; set; }
        public LanguageStyleOptions LanguageStyle { get; set; }
        public string TranslatorName { get; set; }
    }
}

//LanguageId   : Int. (Primary Key)
//TranslationNameInEnglish : Text
//TranslationNameInLocal  ): Text(like in Urdu
//LanguageStyle: Int (Left to right or right to Left .this effects on UI)
//TranslatorName : String