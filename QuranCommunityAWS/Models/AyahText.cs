﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.Models
{
    public class AyahText
    {
        public int Id { get; set; }
        public int AyahNumber { get; set; }
        public String Text { get; set; }
        public int LanguageId { get; set; }
    }
}


//Id(primary key) auto generated
//AyahNumber : (From ayah) Int.Originally ayah number
//Text : Text (the ayah text . It could be in English , Arabic or any language)
//LanguageId  : Int(from language detail.It will help us to get all language data ) 