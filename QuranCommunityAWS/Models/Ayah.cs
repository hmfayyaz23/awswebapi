﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.Models
{
    public class Ayah
    {
        public int Id { get; set; }
        public int AyahNumber { get; set; }
        public int DuaId { get; set; }
        public int SurahNumber { get; set; }
        public int JuzNumber { get; set; }
    }
}

//Id : (Primary Key) auto-generated
//AyahNumber : Int.Originally ayah number(also unique)
//DuaId : int (form dua detail.If dua is present then it will have dua detail id else -1)
//SurahNumber : int (we will able to remove all calculation from client side to show surah detail) 
//JuzNumber : int (we will able to remove all calculation from client side to show juz detail)