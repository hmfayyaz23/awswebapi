﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.Models
{
    public class Juz
    {
        public int Id { get; set; }
        public int JuzNumber { get; set; }
        public string NameInEnglish { get; set; }
        public string NameInArabic { get; set; }
        public int StartAyahId { get; set; }
        public int EndAyahId { get; set; }
        public int StartSurahId { get; set; }
    }
}

//int Id;
//Int JuzNumber;
//String NameInEnglish;
//String NameInArabic;
//int StartAyahId;
//int EndAyahId;
//int StartSurahId;