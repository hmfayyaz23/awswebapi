﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.Models
{
    public class RecommendedVideos
    {
        public int Id { get; set; }
        public string VideoId { get; set; }
        public int AyahNumber { get; set; }
        public int SurahNumber { get; set; }
    }
}


//Id : int
//VideoId : string
//AyahNumber : int (forien key) can be null if this is surah Recommend
//SurahNumber : int (forien key) can be null if this is Ayah Recommend
