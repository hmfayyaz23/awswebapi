﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuranCommunityAWS.Models
{
    public class Surah
    {
        public enum OriginType
        {
            Mecca, Medina
        }

        public int Id { get; set; }
        public int SurahNumber { get; set; }
        public string NameInEnglish { get; set; }
        public string TranslationinEnglish { get; set; }
        public string NameInArabic { get; set; }
        public int StartAyahId { get; set; }
        public int EndAyahId { get; set; }
        public OriginType Origin { get; set; }
    }
}

//int Id;
//int SurahNumber;
//String NameInEnglish;
//String TranslationinEnglish;
//String NameInArabic;
//int StartAyahId;
//int EndAyahId;
//String Origin. (Mecca or Medina)