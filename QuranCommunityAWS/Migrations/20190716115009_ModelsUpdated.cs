﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuranCommunityAWS.Migrations
{
    public partial class ModelsUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AyahText",
                table: "Ayah");

            migrationBuilder.DropColumn(
                name: "LanguageId",
                table: "Ayah");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Surah",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "RecommendedVideos",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "SurahId",
                table: "RecommendedVideos",
                newName: "SurahNumber");

            migrationBuilder.RenameColumn(
                name: "AyahId",
                table: "RecommendedVideos",
                newName: "AyahNumber");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Language",
                newName: "LanguageId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Juz",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "AyahId",
                table: "Dua",
                newName: "AyahNumber");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Dua",
                newName: "DuaId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Ayah",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "JuzNumber",
                table: "Juz",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Dua",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JuzNumber",
                table: "Juz");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Dua");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Surah",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "RecommendedVideos",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "SurahNumber",
                table: "RecommendedVideos",
                newName: "SurahId");

            migrationBuilder.RenameColumn(
                name: "AyahNumber",
                table: "RecommendedVideos",
                newName: "AyahId");

            migrationBuilder.RenameColumn(
                name: "LanguageId",
                table: "Language",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Juz",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "AyahNumber",
                table: "Dua",
                newName: "AyahId");

            migrationBuilder.RenameColumn(
                name: "DuaId",
                table: "Dua",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Ayah",
                newName: "ID");

            migrationBuilder.AddColumn<string>(
                name: "AyahText",
                table: "Ayah",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LanguageId",
                table: "Ayah",
                nullable: false,
                defaultValue: 0);
        }
    }
}
