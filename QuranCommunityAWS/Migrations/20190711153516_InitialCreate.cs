﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuranCommunityAWS.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ayah",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AyahNumber = table.Column<int>(nullable: false),
                    AyahText = table.Column<string>(nullable: true),
                    LanguageId = table.Column<int>(nullable: false),
                    DuaId = table.Column<int>(nullable: false),
                    SurahNumber = table.Column<int>(nullable: false),
                    JuzNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ayah", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Dua",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AyahId = table.Column<int>(nullable: false),
                    Details = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dua", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Juz",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NameInEnglish = table.Column<string>(nullable: true),
                    NameInArabic = table.Column<string>(nullable: true),
                    StartAyahId = table.Column<int>(nullable: false),
                    EndAyahId = table.Column<int>(nullable: false),
                    StartSurahId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Juz", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Language",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TranslationNameInEnglish = table.Column<string>(nullable: true),
                    TranslationNameInLocal = table.Column<string>(nullable: true),
                    LanguageStyle = table.Column<int>(nullable: false),
                    TranslatorName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Language", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RecommendedVideos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    VideoId = table.Column<string>(nullable: true),
                    AyahId = table.Column<int>(nullable: false),
                    SurahId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecommendedVideos", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Surah",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SurahNumber = table.Column<int>(nullable: false),
                    NameInEnglish = table.Column<string>(nullable: true),
                    TranslationinEnglish = table.Column<string>(nullable: true),
                    NameInArabic = table.Column<string>(nullable: true),
                    StartAyahId = table.Column<int>(nullable: false),
                    EndAyahId = table.Column<int>(nullable: false),
                    Origin = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Surah", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ayah");

            migrationBuilder.DropTable(
                name: "Dua");

            migrationBuilder.DropTable(
                name: "Juz");

            migrationBuilder.DropTable(
                name: "Language");

            migrationBuilder.DropTable(
                name: "RecommendedVideos");

            migrationBuilder.DropTable(
                name: "Surah");
        }
    }
}
