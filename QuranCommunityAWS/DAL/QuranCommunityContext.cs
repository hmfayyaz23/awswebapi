﻿using Microsoft.EntityFrameworkCore;
using QuranCommunityAWS.Models;

namespace QuranCommunityAWS.DAL
{
    public class QuranCommunityContext : DbContext
    {
        public QuranCommunityContext(DbContextOptions<QuranCommunityContext> options) : base(options)
        {
        }

        public DbSet<Ayah> Ayahs { get; set; }
        public DbSet<Dua> Duas { get; set; }
        public DbSet<Juz> Juzs { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<RecommendedVideos> RecommendedVideos { get; set; }
        public DbSet<Surah> Surahs { get; set; }
        public DbSet<AyahText> AyahText { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ayah>().ToTable("Ayah");
            modelBuilder.Entity<Dua>().ToTable("Dua");
            modelBuilder.Entity<Juz>().ToTable("Juz");
            modelBuilder.Entity<Language>().ToTable("Language");
            modelBuilder.Entity<RecommendedVideos>().ToTable("RecommendedVideos");
            modelBuilder.Entity<Surah>().ToTable("Surah");
            modelBuilder.Entity<AyahText>().ToTable("AyahText");
        }
    }
}