﻿
namespace QuranCommunityAWS.DAL
{
    public static class DbInitializer
    {
        public static void Initialize(QuranCommunityContext context)
        {
            // 
            context.Database.EnsureCreated();
        }
    }
}